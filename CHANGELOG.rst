CHANGELOG
=========

0.15.1
------

Re-release of 0.15.0 with modernized test/build/deploy jobs.

0.15.0
------

- Add support for compressed (zipped) TLE archives
- Add new source: McCants catalog `classfd.zip`
- Add support for file:// URIs to reference local files.
- Python 3.12 Support

0.14.0
------

- Fixed `#16`_ (Celestrak query URL changed)

.. _`16`: https://gitlab.com/librespacefoundation/python-satellitetle/-/issues/16

0.13.0
------

- Update python library dependencies
- Move from celestrak.com to celestrak.org
- Limit NORAD IDs number per space-track.org request

0.12.0
------

- Update python library dependencies

0.11.1
------

This is a bugfix release.

- Fixed `#15`_ (Deploy job for release 0.11.0 failed)

.. _`15`: https://gitlab.com/librespacefoundation/python-satellitetle/-/issues/15

0.11.0
------

- Fixed `#14`_ (Trailing newline in TLE source breaks fetch_tle_from_url)
- Dropped Python 2.7 support

.. _`#14`: https://gitlab.com/librespacefoundation/python-satellitetle/issues/14

0.10.1
-----

- Fix version in setup.py

0.10.0
-----

- Added `fetch_all_tle` function that fetches and returns all the TLEs from sources for each satellite
- Added `fetch_latest_tle` function that fetches and returns only the latest TLE for each satellite
- Added parameter `only_latest` (`True` by default) in `fetch_tle` function. `fetch_tle` and its `only_latest` parameter are used from both the new functions above

0.9.0
-----

- Fixed `#13`_ (user-configurable TLE sources)
- Added Space-Track.org support
- Added custom sources support
- Fixed and added tests

.. _`#13`: https://gitlab.com/librespacefoundation/python-satellitetle/issues/13

0.8.1
-----

This is a bugfix release.

- Fixed `#12`_ (Trailing newline in TLE source breaks fetch_tle_from_url)

.. _`#12`: https://gitlab.com/librespacefoundation/python-satellitetle/issues/12

0.8.0
-----

This is a bugfix release.

- Fixed `#11`_ (broken fetch_tle_from_celestrak due to Celestrak API changes)
- Fixed tests (failed tests due to new satellites with temporary norad_id < 99900 in SatNOGS DB)
- Added tests for fetch_tle_from_celestrak

.. _`#11`: https://gitlab.com/librespacefoundation/python-satellitetle/issues/11

0.7.0
-----

- Add HTTP requests timeout (20 seconds)

0.6.0
-----

- Add (optional) argument to skip TLS server cert verification
- Replace 'Celestrak (active)' with the (new) 'Celestrak (SatNOGS)' source

0.5.1
-----

- Reduce highest logging level from error to warning

0.5.0
-----

- Add 'Celestrak: active satellites' source, remove all 44 other Celestrak sources
- Add logging with different log levels


0.4.0
-----

- Add error handling to the fetch_tle/fetch_tles methods


0.3.1
-----

- Fix fetch_tles_from_url


0.3.0
-----

First release of python-satellitetle
